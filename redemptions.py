from openpyxl import load_workbook, Workbook
from openpyxl.worksheet.cell_range import CellRange
import sys
import os
import glob

def read():
	for sheet in workbook.sheetnames:
		readsheet(sheet)

def readsheet(sheetname):
	collecting=False
	rowcounter = 1

	sheet=workbook[sheetname]
	for row in sheet.iter_rows():
		cell = row[0]
		if cell.fill.bgColor.value == 'FFD9EAD3':
			startrow = rowcounter
			collecting = True
			cohort=row[0].value

			print ("Processing '%s' for sheet '%s'" % (cohort, sheetname))
			cohort_filename = "output/%s-%s.xlsx" % (currentfile.replace("input/",""), cohort)
			loadfile=currentfile
			if (os.path.exists(cohort_filename)):
				loadfile = cohort_filename
			nw = load_workbook(filename=loadfile)
			ns = nw[sheetname]
		if cell.value == None and collecting == True:
			collecting = False
			endrow = rowcounter - 1
			if startrow > 2:
				numrows = endrow - startrow + 1
				startrange = "A%s:Z%s" % (startrow, endrow)
				amount = -1 * startrow + 2
			
				mrange = "A%s:Z%s" % (2, numrows + 1)
				
				for cellrange in ns.merged_cells.ranges:
					print("\tChecking range %s against %s" % (cellrange, mrange))
					if cellrange.min_row > 2:
						#merging preserves after move.  as such, clear out merge cells 
						#first within our range to copy to
						try:
							intersection = cellrange.intersection(CellRange(mrange))
							print("\t\tUnmerging %s" % intersection)
							ns.unmerge_cells(str(intersection))
						except ValueError as err1:
							pass
				print ("\tMoving from %s to %s" % (startrange, mrange))
				ns.move_range(startrange, rows=amount, translate=True)
			print ("\tDeleting from rows %s to %s" % (endrow-startrow+3, ns.max_row))
			ns.delete_rows(endrow - startrow + 3, ns.max_row)
			nw.save(cohort_filename)
		rowcounter = rowcounter + 1

if __name__ == "__main__":
	if not os.path.isdir("input"):
		sys.exit("Please create an input directory that contains the .xlsx files to process")

	files = glob.glob("input/*.xlsx")
	if len(files) == 0:
		sys.exit("No files to process")

	if not os.path.isdir("output"):
		print("Creating output directory")
		os.mkdir("output")

	for xlsx in files:
		currentfile = xlsx
		print ("Processing %s" % currentfile)
		workbook = load_workbook(filename=currentfile)
		read()

