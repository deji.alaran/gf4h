# gf4h

Processes a list of redemptions in input/ and writes to output/

## Dependencies
- python3
- pip

After installing programs above, run 
- pip install -r requirements.txt

## Usage
- python3 redemptions.py

## Design Notes
- Program reads until it encounters a cell with background colour of FFD9EAD33.  This marks the beginning of a new cohort
- Program assumes there is at least one blank line between cohorts
- Program only works on columns A to Z currently
